const gulp = require('gulp'),
    fs = require('fs'),
    yml = require('js-yaml'),
    rp = require('request-promise-native'),
    yaml = require('gulp-yaml'),
    { basename, extname } = require('path'),
    { promisify } = require('util');

const get_lm_icon = async f => {
    const doc = yml.safeLoad(fs.readFileSync(f, 'utf8'));
    const dir_dst = './dist/ico/'+basename(f, '.yml');

    if (!fs.existsSync(dir_dst)){
        fs.mkdirSync(dir_dst);
    }

    for (const id in doc.src) {
        const url = doc.src[id];
        const dst = dir_dst+'/'+id+extname(url);
        const r = await rp({ url: url, encoding: null });
        await promisify(fs.writeFile)(dst, r);
        console.log(url, '=>',  dst);
    }
};

gulp.task('get-lm-ico', () => {
    const dir = './src/lm_ico_src';
    const files = fs.readdirSync(dir).filter(f => extname(f) === '.yml' );
    for (f of files) {
        get_lm_icon(dir+'/'+f);
    }
});

gulp.task('default', () => {
    gulp.src('./src/*.yml')
      .pipe(yaml({ schema: 'DEFAULT_SAFE_SCHEMA' }))
      .pipe(gulp.dest('./dist'));
});
